import { client } from '../../api/client'

const initialState = []

export default function todosReducer(state = initialState, action) {
    // omit reducer logic
}

// Thunk function
export async function fetchTodos(dispatch, getState) {
    const response = await client.get('/fakeApi/todos')
    dispatch({ type: 'todos/todosLoaded', payload: response.todos })
}