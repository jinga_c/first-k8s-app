import {LOGIN_ACTIVE, TOGGLE_TODO, SET_FILTER, ADD_TODO, RECORD_SELECTED} from './actionTypes';
const LOG_ACTION = "LOG_ACTION"
const REMOVE_TAB = "REMOVE_TAB"
const GET_FILES_FROM_EVALUATION = "GET_FILES_FROM_EVALUATION"
const OPEN_MODAL_WINDOW = "OPEN_MODAL_WINDOW"
let nextTodoId = 0;

export const addTodo = content => ({
    type: ADD_TODO,
    payload: {
        id: ++nextTodoId,
        content,
    }
});

export const toggleTodo = id => ({
    type: TOGGLE_TODO,
    payload: { id  }
});
export const rowClicked = id => ({
    type: LOG_ACTION,
    payload: { id }
});

export const setFilter = filter => ({
    type: SET_FILTER ,
    payload: {filter}
});

export const addClientRecord = (curRecord) => ({
    type: "RECORD_CLIENTS_SELECTED",
    payload: {curRecord}
})
export const removeClientTab = tabId => ({
    type: REMOVE_TAB,
    payload: {tabId}
})
export const getFilesFromEvaluation = evalid => ({
    type: GET_FILES_FROM_EVALUATION,
    payload: {evalid}
})
export const openModalWindow = record => ({
    type: OPEN_MODAL_WINDOW,
    payload: {record}
})

