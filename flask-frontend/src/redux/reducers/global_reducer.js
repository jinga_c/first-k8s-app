const global_reducer = (state = {
    menuItemActive: 'home',
    activeTabKey: '1',
    tabpanels: [
    ],
    toolbarAction: null,
    loading: false,
} , action) => {

    switch (action.type) {
        /*
         LOGIN_START {email: xxx, pass: xxx}
         - UI pending state
         - disable submit button
         - show spinner
         LOGIN_SUCCESS
         { name: XXX, avatar: XX }
         - hide spinner
         - enable submit button
         - add response to UI state

         LOGIN_ERROR

         PERSON_LIST_START
         PERSON_LIST_SUCCESS
         PERSON_LIST_ERROR


         */

        /* Tab Editor */
        case "LOGIN_ACTIVE":
            let tabpanel = action.initpanel;
            return Object.assign({}, state, {
                login_active: tabpanel
            })
        case "CHANGE_ACTIVE_TAB":
            return Object.assign({}, state, {
                activeTabKey: action.activeTabKey
            })

        /*End of Tab Editor */
        case 'ADD_NEW_TAB_ITEM':
            let tabulars = state.tabpanels.slice();
            let lastkey = tabulars[tabulars.length-1].key;
            let newTab = {
                title: 'Tab '+lastkey, content: null, key:  lastkey,
            }
            tabulars.push(newTab);

            return Object.assign({}, state, {
                uiInProgress: true,
                tabpanels: tabulars,
            })
        case 'LOG_ACTION':
            console.log(action)
            return Object.assign({}, state, {
                currentRecord: action
            })

        /* ABSTRACT CALL TO SERVER */
        case 'ABSTRACT_FETCH':
            return Object.assign({}, state, {
                fetchInProgress: true
            })
        case 'ABSTRACT_FETCH_SUCCESS':
            return Object.assign({}, state, {
                fetchInProgress: false,
                abstractResult: action.result,
            })
        case 'ABSTRACT_FETCH_ERROR':
            return Object.assign({}, state, {
                fetchInProgress: false,
                abstractError: action.error,
            })
        /* END OF ABSTRACT CALL TO SERVER */

        case 'MENU_ITEM_ACTIVE':
            return Object.assign({}, state, {
                menuItemActive: action.name,
            })
        case 'SELECTED_ROW':
            return Object.assign({}, state, {
                selectedRow: action.row,
            })

        default:
            return state
    }
}

export default global_reducer