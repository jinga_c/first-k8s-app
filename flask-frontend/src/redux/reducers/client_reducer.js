import {Modal} from 'antd'
export function client_reducer(model) {
    return (state = {
        isLoading: false,
        clientRecords: [],
        totalRecords: 0,
        toolbarAction: null,
        tabIndex: 0,
	backend_addr: '127.0.0.1:5000',
        files: [],
        error: "",
    }, action) => {
        switch (action.type) {
            /*
             LOGIN_START {email: xxx, pass: xxx}
             - UI pending state
             - disable submit button
             - show spinner
             LOGIN_SUCCESS
             { name: XXX, avatar: XX }
             - hide spinner
             - enable submit button
             - add response to UI state

             LOGIN_ERROR

             PERSON_LIST_START
             PERSON_LIST_SUCCESS
             PERSON_LIST_ERROR


             */

            /* Tab Editor */
            case `RECORD_${model}_SELECTED`:
                console.log(action.record)
                let curRecord = action.record
                let new_records = state.clientRecords.slice()
                let arr_index = new_records.indexOf(curRecord)
                let error = ""
                if (arr_index == "-1") {
                    new_records.push(curRecord)
                } else {
                    error = "Tab exists"
                }

                return Object.assign({}, state, {
                    clientRecords: new_records,
                    tabindex: state.tabIndex + 1,
                    error: error
                })
            case "GET_FILES_FROM_EVALUATION":
                let evalid = action.record
                const apiUrl = `http://${state.backend_addr}/v3/files/${evalid}`;
                let new_items = [];
                fetch(apiUrl)
                    .then((response) => response.json())
                    .then((data) => {
                        Object.assign(new_items, data)
                    })
                console.log(new_items)
                return Object.assign({}, state, {
                    files: new_items
                })
            //subscribe to channe
            case `REMOVE_${model}_TAB`:
                let existing_records = state.clientRecords.slice()
                let tab_key = action.record
                let my_index = 0;
                existing_records.map((val, index) => {
                    if (val.id == tab_key) {
                        my_index = existing_records.indexOf(val)
                    }
                })
                let new_recs = existing_records.splice(my_index, 1)

                return Object.assign({}, state, {
                    clientRecords: existing_records
                })

            default:
                return state
        }
    }
}
