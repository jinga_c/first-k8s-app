const navigation_reducer = (state = {
    menuItemActive: 'home',
} , action) => {

    switch (action.type) {
        /*
        AVAILABLE MENUS
         - HOME
         - EVALUATIONS
         - FILES
         - CLIENTS

         */

        /* Tab Editor */
        case "HOME":
            let tabpanel = action.initpanel;
            return Object.assign({}, state, {
                menuItemActive: 'home'
            })
        case "EVALUATIONS":
            return Object.assign({}, state, {
                menuItemActive: 'evaluations'
            })

        /*End of Tab Editor */
        case 'FILES':
            return Object.assign({}, state, {
                menuItemActive: 'files'
            })
        case 'CLIENTS':
            return Object.assign({}, state, {
                menuItemActive: 'clients'
            })
        default:
            return state
    }
}

export default navigation_reducer