import {combineReducers} from 'redux';
import filters_reducer from './filters_reducer';
import global_reducer from './global_reducer'
import {client_reducer} from './client_reducer'

const rootReducer =  combineReducers({
    global_reducer,
    clients: client_reducer("CLIENTS"),
    evaluations: client_reducer("EVALUATIONS"),
    files: client_reducer("FILES")
});

export default rootReducer;

