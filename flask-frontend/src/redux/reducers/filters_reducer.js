const filters_reducer = (state = {
    activeFilters: [],
    activeTabKey: '1',
    tabpanels: [
    ],
    toolbarAction: null,
} , action) => {

    switch (action.type) {
        /*
         LOGIN_START {email: xxx, pass: xxx}
         - UI pending state
         - disable submit button
         - show spinner
         LOGIN_SUCCESS
         { name: XXX, avatar: XX }
         - hide spinner
         - enable submit button
         - add response to UI state

         LOGIN_ERROR

         PERSON_LIST_START
         PERSON_LIST_SUCCESS
         PERSON_LIST_ERROR


         */

        /* Tab Editor */
        case "DATE_BETWEEN":
            let tabpanel = action.initpanel;
            return Object.assign({}, state, {
                login_active: tabpanel
            })
        case "CLIENT":
            return Object.assign({}, state, {
                activeTabKey: action.activeTabKey
            })

        default:
            return state
    }
}

export default filters_reducer