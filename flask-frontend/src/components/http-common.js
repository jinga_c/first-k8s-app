import axios from "axios";

export default axios.create({
    baseURL: "http://localhost:5000/v3",
    headers: {
        "Content-type": "application/json"
    }
});