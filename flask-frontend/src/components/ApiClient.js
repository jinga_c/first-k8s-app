import axios from 'axios'
export default async function ApiClientGet(url, params=null) {
    const response = await axios.get(url, {
            params: params
        }).then(function(response){
            console.log(response) //Dispatch an action
        }).catch(function(err){
            console.log(err)
        }).then(function(){
            console.log("Function ApiClient executed");
        })
    }
