import React, {Component} from 'react'
import {connect} from 'react-redux'
import 'antd/dist/antd.css';
import {Button, Dropdown, Menu, Table, Tabs} from 'antd';
import ApiClientGet from "../ApiClient";
import Icon from "@ant-design/icons";
const {TabPane} = Tabs

class Files extends Component {
    constructor(props){
        super(props);

        this.state = {
            items: [],
            pagination: {
                current: 1,
                pageSize: 10,
            },
	    backend_addr: props.backend_addr, 
            loading: false,
        };
    };
    componentDidMount() {
        let pagination = this.state.pagination;
        const apiUrl = `http://${this.state.backend_addr}/v3/files?current=${this.state.pagination.current}&pageSize=${this.state.pagination.pageSize}`;
        let new_items = [];
        fetch(apiUrl)
            .then((response) => response.json())
            .then((data) => {
                new_items.push(data);
                let response_data = data;
                console.log(new_items);
                this.setState({
                    items: data.files
                })
            })
        //subscribe to channe



    };
    handleTableChange = (pagination, filters, sorter) => {
        this.fetch({
            sortField: sorter.field,
            sortOrder: sorter.order,
            pagination,
            ...filters,
        });
    };
    filtersMenu = () => {
        return (
            <Menu onClick={this.applyFilter}>
                <Menu.Item key="1">Find all files from basket</Menu.Item>
                <Menu.Item key="2">Find all files in date range</Menu.Item>
            </Menu>
        )
    }
    onEdit = (targetKey, action) => {
        if(action == "remove"){
            this.props.removeClientTab(targetKey)
        }
    };
    render() {
        const columns = [
            {
                title: 'id',
                key: 'id',
                dataIndex: 'id',
                sorter: true,
                width: '20%',
            },
            {
                title: 'uuid_f',
                dataIndex: 'uuid_f',
                key: 'uuid_f',
                width: '20%',
            },
            {
                title: 'status',
                dataIndex: 'status',
                key: 'status',
                width: '20%',
            },
            {
                title: 'score',
                dataIndex: 'score',
                key: 'score',
                width: '20%',
            },
            {
                title: 'message',
                dataIndex: 'message',
                key: 'message',
                width: '20%',
            },
        ];
        const paginationObj = {
            showQuickJumper: true,
            defaultCurrent: 2,
            total: this.state.items.totalItems,
            onChange: this.onChage
        };
        let rows = this.state.items
        return (
            <React.Fragment>
                <div className="card-container">
                    <Button.Group>
                        <Dropdown overlay={this.filtersMenu}>
                            <Button>
                                Filters <Icon type="down" />
                            </Button>
                        </Dropdown>
                    </Button.Group>
                    <Tabs type="editable-card" hideAdd onEdit={this.onEdit}>
                        <TabPane tab="Files" key="99" closable={false} >
                            <Table rowKey={ rows => rows.id } columns={columns} dataSource={rows} pagination={paginationObj}
                                   onRow={(record, rowIndex) => {
                                       return {
                                           onClick: event => {this.props.addClientRecord(record)}, // click row
                                           onDoubleClick: event => {}, // double click row
                                           onContextMenu: event => {console.log(record)}, // right button click row
                                           onMouseEnter: event => {}, // mouse enter row
                                           onMouseLeave: event => {}, // mouse leave row
                                       };
                                   }}/>
                        </TabPane>
                        {this.props.files.clientRecords.map( (record) =>
                            <TabPane tab={record.id} key={record.id} >
                                <p>{record.uuid_f}</p>
                                <p>{record.status}</p>
                                <p>{record.created_at}</p>
                            </TabPane>
                        )}
                    </Tabs>
                </div>
            </React.Fragment>
        );
    }
}
function mapDispatchToProps(dispatch){
    return {
        addClientRecord: (curRecord) => dispatch({type: "RECORD_FILES_SELECTED", record: curRecord}),
        removeClientTab: (curRecord) => dispatch({type: "REMOVE_FILES_TAB", record: curRecord}),
    }
}
function mapStateToProps(state){
    return {
        files: state.files
    }
}
export default connect (
    mapStateToProps,
    mapDispatchToProps
)(Files);
