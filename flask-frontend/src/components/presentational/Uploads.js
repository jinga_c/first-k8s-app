import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import { Upload, Button} from 'antd';
import Icon from '@ant-design/icons'
import {connect} from 'react-redux'


class UploadFiles extends React.Component {
    state = {
        fileList: [ ],
    };

    handleChange = info => {
        let fileList = [...info.fileList];


        // 2. Read from response and show file link
        fileList = fileList.map(file => {
            if (file.response) {
                // Component will show file.url as link
                file.url = file.response.url;
            }
            return file;
        });

        this.setState({ fileList });
    };

    render() {
        const props = {
            action: 'http://localhost:5000/v3/eval',
            onChange: this.handleChange,
            multiple: true,
        };
        return (
            <Upload {...props} fileList={this.state.fileList}>
                <Button>
                    <Icon type="upload" /> Upload
                </Button>
            </Upload>
        );
    }
}

export default connect()(UploadFiles)