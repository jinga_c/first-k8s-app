import React, {Component} from 'react'
import {connect} from 'react-redux'
import DemoPie from './charts/Pie'
import DemoColumn from './charts/DemoColumn'
import DemoRose from "./charts/DemoRose";
import {Row,Col} from 'antd'
import DemoHistogram from "./charts/Histogram";
class Home extends Component {
    constructor(props){
        super(props);
    }

    render() {
        return (
            <div className="Content-Class">
                <Row>
                <Col span={24}>
                        <div className="UploadedFiles">
                        <p>Send files by user </p>
                        <DemoPie />
                        </div>
                </Col>
                    <Col span={24}>
                        <div className="sendFiles">
                            <p>Send Files per month</p>
                            <DemoColumn />
                        </div>
                    </Col>
                </Row>

            </div>
        )
    }
}

export default connect (
    null,
    null,
)(Home);