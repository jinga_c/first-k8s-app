import React, {Component} from 'react'
import {connect} from 'react-redux'
import 'antd/dist/antd.css';
import { message,Table, Tabs, Alert, Button, Menu, Dropdown } from 'antd';
import Icon from '@ant-design/icons'
import {addClientRecord, removeClientTab} from "../../redux/actions";
import ApiClientGet from "../ApiClient";

const {TabPane} = Tabs

class AbstractTable extends Component {
    constructor(props){
        super(props);

        this.state = {
            items: [],
            pagination: {
                current: 1,
                pageSize: 100,
            },
	    backend_addr: props.backend_addr,
            totalItems: 0,
            loading: false,

        };
    };
    componentDidMount() {

        let pagination = this.state.pagination;
        const apiUrl = `http://${this.state.backend_addr}/v3/clients?current=${this.state.pagination.current}&pageSize=${this.state.pagination.pageSize}`;
        let new_items = [];
        fetch(apiUrl)
            .then((response) => response.json())
            .then((data) => {
                new_items.push(data);
                let response_data = data;
                console.log(data.clients);
                this.setState({
                    items: data,
                });
            })
        //subscribe to channe



    };
}
class Clients extends Component {
    constructor(props){
        super(props);

        this.state = {
            items: [],
            pagination: {
                current: 1,
                pageSize: 100,
            },
	    backend_addr: props.backend_addr,
            totalItems: 0,
            loading: false,
        };
    };
    componentDidMount() {

        let pagination = this.state.pagination;
        const apiUrl = `http://${this.state.backend_addr}/v3/clients?current=${pagination.current}&pageSize=${pagination.pageSize}`;
        let new_items = [];
        fetch(apiUrl)
            .then((response) => response.json())
            .then((data) => {
                new_items.push(data);
                let response_data = data;
                console.log(data.clients);
                this.setState({
                    items: data,
                });
            })
        //subscribe to channe



    };
    handleTableChange = (pagination, filters, sorter) => {
        this.fetch({
            sortField: sorter.field,
            sortOrder: sorter.order,
            pagination,
            ...filters,
        });
    };
    onChange = activeKey => {
        console.log(activeKey)
        this.setState({ activeKey });
    };

    onEdit = (targetKey, action) => {
        if(action == "remove"){
            this.props.removeClientTab(targetKey)
        }
    };
    error = (my_message) => {
        message.error(my_message);
    };
    applyFilter = (my_filter) => {
        console.log(my_filter)
    }
    filtersMenu = () => {
        return (
            <Menu onClick={this.applyFilter}>
                <Menu.Item key="1">Find All Baskets by User</Menu.Item>
                <Menu.Item key="2">Find All Files by User</Menu.Item>
            </Menu>
        )
    }

    render() {
        const columns = [
            {
                title: 'id',
                key: 'id',
                dataIndex: 'id',
                sorter: true,
                width: '20%',
            },
            {
                title: 'name',
                dataIndex: 'name',
                key: 'name',
                width: '20%',
            },
            {
                title: 'pseudoname',
                dataIndex: 'pseudoname',
                key: 'pseudoname',
                width: '20%',
            },
            {
                title: 'created_at',
                dataIndex: 'created_at',
                key: 'created_at',
                width: '20%',
            },
            {
                title: 'last_action_date',
                dataIndex: 'last_action_date',
                key: 'last_action_date',
                width: '20%',
            },
        ];

        let rows = this.state.items.clients
        const paginationObj = {
            showQuickJumper: true,
            defaultCurrent: 1,
            total: this.state.items.totalItems,
            onChange: this.onChage
        }


        return (
            <React.Fragment>
              <div className="card-container">
                  <Button.Group>
                      <Dropdown overlay={this.filtersMenu}>
                          <Button>
                              Filters <Icon type="down" />
                          </Button>
                      </Dropdown>
                  </Button.Group>
                  <Tabs type="editable-card" hideAdd onEdit={this.onEdit}>
                      <TabPane tab="Clients" key="99" closable={false} >
                        <Table rowKey={ rows => rows.id } columns={columns} dataSource={rows} pagination={paginationObj}
                               onRow={(record, rowIndex) => {
                                   return {
                                       onClick: event => {this.props.addClientRecord(record)}, // click row
                                       onDoubleClick: event => {}, // double click row
                                       onContextMenu: event => {console.log(record)}, // right button click row
                                       onMouseEnter: event => {}, // mouse enter row
                                       onMouseLeave: event => {}, // mouse leave row
                                   };
                               }}/>
                      </TabPane>
                      {/* If selected record of client is present then for each reacord add a new TabPane with contents of record */}
                      {this.props.clientReducer.clientRecords.map( (record) =>
                          <TabPane tab={record.name} key={record.id} >
                              <p> {record.name} - {record.common_name} </p>
                          </TabPane>
                      )}

                  </Tabs>
              </div>
            </React.Fragment>
        );
    }
}
function mapDispatchToProps(dispatch){
    return {
        addClientRecord: (curRecord) =>  dispatch({type: "RECORD_CLIENTS_SELECTED", record: curRecord}),
        removeClientTab: (curRecord) => dispatch({type: "REMOVE_CLIENTS_TAB", record: curRecord}),
    }
}
function mapStateToProps(state){
    return {
        clientReducer: state.clients
    }
}
export default connect (
    mapStateToProps,
    mapDispatchToProps
)(Clients);
