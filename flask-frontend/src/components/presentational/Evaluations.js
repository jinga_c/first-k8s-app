import React, {Component} from 'react'
import {connect} from 'react-redux'
import 'antd/dist/antd.css';
import { message,Table, Tabs, Alert, Button, Menu, Dropdown, Modal } from 'antd';
import Icon from '@ant-design/icons'
import {addClientRecord, removeClientTab, getFilesFromEvaluation, openModalWindow} from "../../redux/actions";

const {TabPane} = Tabs

class Evaluations extends Component {
    constructor(props) {
        super(props);

        this.state = {
            items: [],
            pagination: {
                current: 1,
                pageSize: 10,
            },
	    backend_addr: props.backend_addr,
            loading: false,
            showModal: false,
        };
    };

    componentDidMount() {
        let pagination = this.state.pagination;
        const apiUrl = `http://${this.state.backend_addr}/v3/eval/params?current=${this.state.pagination.current}&pageSize=${this.state.pagination.pageSize}`;
        let new_items = [];
        fetch(apiUrl)
            .then((response) => response.json())
            .then((data) => {
                new_items.push(data);
                let response_data = data;
                console.log(new_items);
                this.setState({
                    items: data
                })
            })
        //subscribe to channe


    };

    handleTableChange = (pagination, filters, sorter) => {
        this.fetch({
            sortField: sorter.field,
            sortOrder: sorter.order,
            pagination,
            ...filters,
        });
    };
    onChange = activeKey => {
        console.log(activeKey)
        this.setState({ activeKey });
    };

    onEdit = (targetKey, action) => {
        if(action == "remove"){
            this.props.removeClientTab(targetKey)
        }
    };
    error = (my_message) => {
        message.error(my_message);
    };
    applyFilter = (my_filter) => {
        console.log(my_filter)
    }
    filtersMenu = () => {
        return (
            <Menu onClick={this.applyFilter}>
                <Menu.Item key="1">find all baskets by User</Menu.Item>
                <Menu.Item key="2">find all baskets in date range</Menu.Item>
            </Menu>
        )
    }
    getFilesFromEvaluation(recid)
    {
        const apiUrl = `http://${this.state.backend_addr}/v3/files/${recid}`;
        let new_items = [];
        fetch(apiUrl)
            .then((response) => response.json())
            .then((data) => {
                new_items.push(data);
                let response_data = data;
                console.log(new_items);
                this.setState({
                    files: data
                })
            })
    }
    showFileRecordModal = (record) => {
        Modal.info({
            title: 'record.name',
            content: (
                <div>
                    <p>{record.id}</p>
                    <p style={{color: 'red'}}>{record.uuid_f}</p>
                    <p>{record.message}</p>
                    <p >{record.results}</p>
                </div>
            ),
            onOk() {},
        });
    }
    render() {
        let rows = this.state.items.evals
        let eval_files = this.props.evaluations.files
        const paginationObj = {
            showQuickJumper: true,
            defaultCurrent: 2,
            total: this.state.items.totalItems,
            onChange: this.onChage
        }
        const columns = [
            {
                title: 'id',
                key: 'id',
                dataIndex: 'id',
                sorter: true,
                width: '20%',
            },
            {
                title: 'uuid_f',
                dataIndex: 'uuid_f',
                key: 'uuid_f',
                width: '20%',
            },
            {
                title: 'status',
                dataIndex: 'status',
                key: 'status',
                width: '20%',
            },
            {
                title: 'score',
                dataIndex: 'score',
                key: 'score',
                width: '20%',
            },
        ];
        const childColumns = [
            {
                title: 'id',
                key: 'id',
                dataIndex: 'id',
                sorter: true,
                width: '20%',
            },
            {
                title: 'uuid_f',
                dataIndex: 'uuid_f',
                key: 'uuid_f',
                width: '20%',
            },
            {
                title: 'status',
                dataIndex: 'status',
                key: 'status',
                width: '20%',
            },
            {
                title: 'score',
                dataIndex: 'score',
                key: 'score',
                width: '20%',
            },
            {
                title: 'results',
                dataIndex: 'results',
                key: 'results',
                width: '20%',
            },
        ]

        return (
            <React.Fragment>
                <div className="card-container">
                    <Button.Group>
                        <Dropdown overlay={this.filtersMenu}>
                            <Button>
                                Filters <Icon type="down"/>
                            </Button>
                        </Dropdown>
                    </Button.Group>
                    <Tabs type="editable-card" hideAdd onEdit={this.onEdit}>
                        <TabPane tab="Evaluations" key="99" closable={false}>
                            <Table rowKey={rows => rows.id} columns={columns} dataSource={rows}
                                   pagination={paginationObj}
                                   onRow={(record, rowIndex) => {
                                       return {
                                           onClick: event => {
                                               this.props.addClientRecord(record)
                                               this.props.getFilesFromEvaluation(record.id)
                                           }, // click row
                                           onDoubleClick: event => {
                                           }, // double click row
                                           onContextMenu: event => {
                                               console.log(record)
                                           }, // right button click row
                                           onMouseEnter: event => {
                                           }, // mouse enter row
                                           onMouseLeave: event => {
                                           }, // mouse leave row
                                       };
                                   }}/>
                        </TabPane>
                        {/* If selected record of client is present then for each reacord add a new TabPane with contents of record */}
                        {this.props.evaluations.clientRecords.map((record) =>
                            <TabPane tab={record.id} key={record.id}>
                                <p>{record.uuid_f} - {record.score}</p>
                            </TabPane>
                        )}

                    </Tabs>
                </div>
            </React.Fragment>
        );
    }
}
function mapDispatchToProps(dispatch){
    return {
        addClientRecord: (curRecord) => dispatch({type: "RECORD_EVALUATIONS_SELECTED", record: curRecord}),
        removeClientTab: (curRecord) => dispatch({type: "REMOVE_EVALUATIONS_TAB", record: curRecord}),
        getFilesFromEvaluation: (curRecord) => dispatch({type: "GET_FILES_FROM_EVALUATION", record: curRecord}),
    }
}
function mapStateToProps(state){
    return {
        evaluations: state.evaluations
    }
}
export default connect (
    mapStateToProps,
    mapDispatchToProps,
)(Evaluations);
