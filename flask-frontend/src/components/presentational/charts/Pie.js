import React, { useState, useEffect } from 'react';
import { Pie } from '@ant-design/charts';

const DemoPie: React.FC = () => {
    var data = [
        {
            type: 'user1',
            value: 7,
        },
        {
            type: 'user2',
            value: 15,
        },
        {
            type: 'user3',
            value: 3,
        },
        {
            type: 'user4',
            value: 6,
        },
        {
            type: 'user5',
            value: 4,
        },

    ];
    var config = {
        appendPadding: 10,
        data: data,
        angleField: 'value',
        colorField: 'type',
        radius: 0.9,
        label: {
            type: 'inner',
            offset: '-30%',
            style: {
                fontSize: 14,
                textAlign: 'center',
                display: 'block'
            },
        },
        legend: {position: 'bottom'},
        interactions: [{ type: 'pie-legend-active' }, {type: 'element-active'}],
    };
    return <Pie {...config} width={250} heigh={250} />;
};
export default DemoPie;