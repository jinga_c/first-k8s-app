import React, { useState, useEffect } from 'react';
import { Column } from '@ant-design/charts';
const DemoColumn: React.FC = () => {
    var data = [
        {
            type: 'January',
            value: 10,
        },
        {
            type: 'February',
            value: 11,
        },
        {
            type: 'March',
            value: 12,
        },
        {
            type: 'April',
            value: 13,
        },
        {
            type: 'June',
            value: 12,
        },
        {
            type: 'July',
            value: 20,
        },
        {
            type: 'August',
            value: 15,
        },
        {
            type: 'September',
            value: 15,
        },
        {
            type: 'October',
            value: 15,
        },
        {
            type: 'November',
            value: 15,
        },
        {
            type: 'December',
            value: 15,
        },

    ];
    var paletteSemanticRed = '#F4664A';
    var brandColor = '#5B8FF9';
    var config = {
        data: data,
        xField: 'type',
        yField: 'value',
        seriesField: '',
        color: function color(_ref) {
            var type = _ref.type;
            if (type === 'Saturday' || type === 'Sunday') {
                return paletteSemanticRed;
            }
            return brandColor;
        },
        label: {
            content: function content(originData) {
                var val = parseFloat(originData.value);
                if (val < 50) {
                    return (val * 100).toFixed(1) + '%';
                }
            },
            offset: 10,
        },
        legend: false,
        meta: {
            type: { alias: '类别' },
            sales: { alias: '销售额' },
        },
    };
    return <Column {...config}  width={250} height={250}/>;
};
export default DemoColumn;