import React, { useState, useEffect } from 'react';
import { Rose } from '@ant-design/charts';
const DemoRose: React.FC = () => {
    var data = [
        {
            type: 'user1',
            value: 2700,
        },
        {
            type: 'user2',
            value: 2500,
        },
        {
            type: 'user3',
            value: 1800,
        },
        {
            type: 'user4',
            value: 1500,
        },
        {
            type: 'user5',
            value: 1000,
        },
        {
            type: 'user6',
            value: 5000,
        },
        {
            type: 'user7',
            value: 1500,
        },
    ];
    var config = {
        data: data,
        xField: 'type',
        yField: 'value',
        seriesField: 'type',
        radius: 0.9,
        legend: { position: 'right' },
    };
    return <Rose {...config} width={300} height={300}/>;
};
export default DemoRose;