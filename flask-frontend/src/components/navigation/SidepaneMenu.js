import React, {Component} from 'react'
import {Link,Switch, Route, } from 'react-router-dom'
import {Menu, Button} from 'antd'
import 'antd/lib/menu/style/index.css'
import Evaluations from '../presentational/Evaluations'
import Files from '../presentational/Files'
import Clients from '../presentational/Clients'
import Home from '../presentational/Home'

import PropTypes from 'prop-types';


class SidepaneMenu extends React.Component {
    constructor(props){
        super(props);
        console.log(this)

    }

    render() {
        return (
            <div className="sidepane-menu">
                <Menu style={{width: 150}} mode="vertical">
                    <Menu.Item> <Link to="/"> Statistics </Link></Menu.Item>
                    <Menu.Item> <Link to="/Evaluations"> Evaluations </Link> </Menu.Item>
                    <Menu.Item> <Link to="/Files"> Files </Link></Menu.Item>
                    <Menu.Item> <Link to="/Clients"> Users </Link></Menu.Item>
                    <Menu.Item> <Link to="/Uploads"> Uploads </Link></Menu.Item>
                </Menu>
            </div>
        )
    }
}

SidepaneMenu.propTypes = {
    menuItemActive: PropTypes.bool
}
export default SidepaneMenu
