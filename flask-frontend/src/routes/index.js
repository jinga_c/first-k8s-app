import React, {Component} from 'react'
import { BrowserRouter as Router, Route, Switch, Redirect, Link} from 'react-router-dom'
import {connect} from 'react-redux'
import MwApp from '../App'




class RouterMain extends Component {
    constructor(props){
        super(props);
    }

    render() {
        { console.log(this) }
        return (

            <Router>
                <div>
                    <Route
                        path="/"
                        component={MwApp} />
                </div>
            </Router>
        )
    }
}

export default RouterMain;