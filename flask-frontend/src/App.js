import logo from './logo.svg';
import React, {Component} from 'react'
import './App.css';
import SidepaneMenu from "./components/navigation/SidepaneMenu";
import {Row,Col} from 'antd'
import {Route, Switch, WithRouter} from "react-router-dom";
import Home from "./components/presentational/Home";
import Evaluations from "./components/presentational/Evaluations";
import Files from "./components/presentational/Files";
import Clients from "./components/presentational/Clients";
import UploadFiles from "./components/presentational/Uploads";
import {connect} from 'react-redux'
require('dotenv').config()
const backend_url='localhost:5000'

class MwApp extends Component {
  constructor(props){
    super(props);
  }
  render() {
    return (
        <div>
          <Row>
            <Col span={24}>
              <h1>Logo and title</h1>
            </Col>
          </Row>
          <Row>
              <Col span={3}>
                  <SidepaneMenu />
              </Col>
              <Col span={20} >
                  <Switch>
                      <Route exact path='/' component={Home} />
                      <Route path='/Evaluations' component={() => <Evaluations backend_addr={backend_url} /> } />
                      <Route path='/Files' component={() => <Files backend_addr={backend_url} /> } />
                      <Route path='/Clients' component={()=> <Clients backend_addr={backend_url} /> } />
                      <Route path='/Uploads' component={UploadFiles} />
                  </Switch>
              </Col>
          </Row>

        </div>


    );
  }
}
export default MwApp;
