from datetime import datetime
from models1 import MyUser, Evaluation, File, Base, session, engine
from random import randint
from time import time
import json, os
import hashlib, pdb
from flask import request, jsonify, session, current_app
import jsonpickle
from uuid import uuid4
import requests
from celery import shared_task

def dbInit():
    Base.metadata.create_all(engine);
#    Base.metadata.drop_all(engine)
def dbDropExisting():
    Base.metadata.drop_all(engine)

dbDropExisting()
dbInit()
