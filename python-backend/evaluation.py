from datetime import datetime
from models1 import MyUser, Evaluation, File, Base, session, engine
from random import randint
from time import time
import json, os
import hashlib, pdb
from flask import request, jsonify, session, current_app
import jsonpickle
from uuid import uuid4
import requests
from celery import shared_task

@shared_task(bind=True, ignore_result=False)
def sendFile(self,f_stream,f_uuid):
    ma_file = {str(f_uuid): (str(f_uuid), f_stream.stream.read(), 'application/octet-stream') }
    scan_url = "http://localhost:5001/"
    headers  = {"X-API-TOKEN":"asdfa8sd7fa98s6df98asdf698asdf"}
    r = requests.post(scan_url, files=ma_file, verify=False, headers=headers,timeout=1)
    if r.ok:
        return  self.update_state(state='SUCCESS')
    return self.update_state(state='FAILURE')

def dbInit():
    Base.metadata.create_all(engine);
#    Base.metadata.drop_all(engine);

def getAll():
    from models1 import session
    evaluations = session.query(Evaluation).all()
#    evaluations = session.query(Evaluation).with_entities(func.count()).scalar()
    evals = []
    evals = ["abc", ["abd","abe"], "abd", ["abd","abe"]]
    evals_dict = {}

    for i in evaluations:
        evals.append(i.as_dict())
    return jsonify(evals)
def getFilesFromEvaluation(evalid):
    from models1 import session
    evaluation = session.query(Evaluation).filter_by(id = evalid).first()
    files = evaluation.file
    results = []
    for i in files:
         results.append(i.as_dict())
    return results
def getEvaluationsByParams():
    from models1 import session
    current =  request.values.get('current')
    pagesize = request.values.get('pageSize')
    evaluations = session.query(Evaluation).filter(Evaluation.id.between(int(current), int(pagesize))).all()
    num_rec_returned = len(evaluations)
    evals = []
    for i in evaluations:
        evals.append(i.as_dict())
    return jsonify({"evals": evals, "num_recs_returned":num_rec_returned})
def getFiles():
    from models1 import session
    current =  request.values.get('current')
    pagesize = request.values.get('pageSize')
    Files = session.query(File).filter(File.id.between(int(current), int(pagesize))).all()
    num_rec_returned = len(Files)
    fls = []
    for i in Files:
        fls.append(i.as_dict())
    return jsonify({"files": fls, "num_recs_returned":num_rec_returned})

def getClients():
    from models1 import session
    current =  request.values.get('current')
    pagesize = request.values.get('pageSize')
    Users = session.query(MyUser).filter(MyUser.id.between(int(current), int(pagesize))).all()
    num_rec_returned = len(Users)
    fls = []
    for i in Users:
        fls.append(i.as_dict())
    return jsonify({"clients": fls, "num_recs_returned":num_rec_returned})

def getFileBySha256(sha256):
    file_obj = session.query(File).filter(File.sha256==sha256).order_by(File.id.desc()).first()
    return json.dumps(file_obj)
def submitEvaluation(corID=None):
    from models1 import session
    import pdb
    fname = "jsmith";
    files_in = request.files.getlist('file')
    files = []
    for f in files_in:
        f.seek(0,os.SEEK_END)
        f_len = f.tell()
        if f_len > 0:
            f.seek(0,os.SEEK_SET)
            files.append(f)
    if len(files) == 0:
        return 411
    #Files array acquired TODO: loop through every file
    my_eval = Evaluation()

#    dir_path = "/tmp/uploads/files/{}".format(my_eval.uuid_f)
#    if not os.path.exists(dir_path):
#        os.makedirs(dir_path);
    import string, random
    letters = string.ascii_lowercase
    my_string=''.join(random.choice(letters) for i in range(10))
    myuser = MyUser(fname,my_string)
#    my_sha1=hashlib.sha1(randint(0,10000))
#    my_file=File("file{}".format(randint(0,100),my_sha1.hexdigest()))
    my_eval.created=myuser
    for fd in files:
        try:
            f_uuid = uuid4()
        except: 
            print("Cannot create f_uuid")
        try:
            my_file = File(f_uuid) # Create a new record
        except: 
            print("Cannot create file record")
        try:
            sendFile(fd, f_uuid) # send file for evaluation
        except:
            print("cannot send file")
        try:
            my_eval.file.append(my_file) #send file for evaluation
        except:
            print("cannot add file to evaluation");
    session.add(my_eval)
    session.commit()
    return 201

