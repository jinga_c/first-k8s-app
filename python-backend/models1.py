# coding=utf-8
from sqlalchemy import Table, Column, Integer, ForeignKey, String, MetaData, DateTime, Text, Float, Boolean, JSON
from sqlalchemy.orm import relationship, sessionmaker, backref
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from time import time
from uuid import uuid4
import json, pdb, os

dbhost=os.environ.get("DB_HOST", "localhost")
dbuser=os.environ.get("DB_USER","yavor")
dbpass=os.environ.get("DB_PASS","yavor")
dbname=os.environ.get("DB_NAME","yavor")
dbport=os.environ.get("DB_PORT","5432")
connect_string=f"postgresql://{dbuser}:{dbpass}@{dbhost}:{dbport}/{dbname}"
engine = create_engine(connect_string, echo=True)

Session=sessionmaker(bind=engine)
session=Session()
meta=MetaData()

Base = declarative_base()
file_evaluation_association = Table(
    'file_evaluations', Base.metadata,
    Column('file_id', Integer, ForeignKey('file.id', ondelete='CASCADE')),
    Column('evaluation_id', Integer, ForeignKey('evaluation.id', ondelete='CASCADE'))
        )
class Evaluation(Base):
    __tablename__ = "evaluation"

    id = Column(Integer, primary_key=True)
    uuid_f=Column(String(128))
    correlation = Column(String(128))
    status = Column(Integer)
    created_at=Column(String(32))
    completed_at = Column(String(128))
    score = Column(Integer)
    status=Column(Integer)
    created_by=Column(Integer, ForeignKey('my_user.id', ondelete='CASCADE'))
    created=relationship("MyUser", passive_deletes=True)
    file=relationship("File",secondary=file_evaluation_association, passive_deletes=True)
# TODO: add relationship * - 1 to file

    def __init__(self, correlation=None, completed_at=None, score=None):
        self.uuid_f=uuid4()
        self.created_at=time()
        self.correlation = correlation
        self.status = 0
        self.completed_at=completed_at
        self.score=score

    def __iter__(self):
        return self.to_dict().iteritems()

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class File(Base):
    __tablename__="file"

    id = Column(Integer, primary_key=True)
    uuid_f = Column(String(36), unique=True)
    status = Column(Integer)
    created_at = Column(String(64))
    completed_at = Column(String(64))
    score = Column(Integer)
    message = Column(Text)


    def __init__(self,uuid_f=None ):
        self.uuid_f = uuid_f
        self.created_at = time()
    def __iter__(self):
        return self.to_dict().iteritems()
    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class MyUser(Base):
    __tablename__="my_user"

    id=Column(Integer,primary_key=True)
    name=Column(String(64))
    pseudoname=Column(String(32))
    fingerprint = Column(String(255), unique=True)
    last_ip = Column(String(32))
    created_at = Column(String(32))
    last_action_date = Column(String(32))


    def __init__(self,name,pseudoname):
        self.name=name
        self.pseudoname=pseudoname
    def __iter__(self):
        return self.to_dict().iteritems()
    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


